# Blinky_Makefile

This project was generated using STM32CubeMX using Toolchain "Makefile". 

It can be 
* build using arm-none-eabi toolchain which is included in STM32CubeIDE or can be downloaded separately
* used as a template for your own Makefile projects without using STM tools
* easily adapted to or re-generated for different MCUs and boards

## Build and Run under Windows

Open a cmd console window and navigate to the project folder.

Set a environment variable (the path may vary for your system):

`set GCC_PATH=c:/st/stm32cubeide_1.1.0/stm32cubeide/plugins/com.st.stm32cube.ide.mcu.externaltools.gnu-tools-for-stm32.7-2018-q2-update.win32_1.0.0.201904181610/tools/bin/`

Execute make (the path may vary for your system):

`C:\ST\STM32CubeIDE_1.1.0\STM32CubeIDE\plugins\com.st.stm32cube.ide.mcu.externaltools.make.win32_1.1.0.201910081157\tools\bin\make.exe`

An USB attached NUCLEO-F042K6 board can pe programmed (flased) with resulting binary `build/Blinky_Makefile.bin` 
simply by copying the binary to the USB "stick" (USB mass storage class device) NODE_F042K6 which comes with the USB interface of that board.

## Build and Run under Ubuntu

Same as above, but install the neccessary packages:

`sudo apt install gcc-arm-none-eabi`
`sudo apt install build-essential`

## Notes
* You can install the arm-none-eabi toolchain and make for Windows wihtout needing STM32CubeIDE as well
* [Debugging](Debugging) needs some extra efforts.
